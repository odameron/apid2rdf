Converter for the APID (https://apid.dep.usal.es/) proteins interactome datasets into RDF.

The reference for the PSI-MI tab format was retrived from https://code.google.com/archive/p/psimi/wikis/PsimiTabFormat.wiki

This project depends on the generic [mitab2rdf](https://gitlab.com/odameron/mitab2rdf) converter.


# Todo

- [ ] specify the datafile as an argument
- [ ] handle correctly the | separator for fields
    - [ ] 1. Unique identifier for interactor A
    - [ ] 2. Unique identifier for interactor B
    - [ ] 5. Aliases for A
    - [ ] 6. Aliases for B
    - [ ] 7. Interaction detection methods
    - [ ] 8. First author
    - [ ] 9. Identifier of the publication
- [ ] improve the design of the relation between the reified apid:Interaction and the two interacting proteins
    -  having one single apid:interactor property used twice for associating an apid:Interaction with its two up:Proteins can lead to confusion between interactions between two instances of the same proteins and interactions between instances from different proteins
    - [ ] define a general apid:interactor property and two sub-properties apid:interactor1 and apid:interactor2.
- [ ] handle correctly the method type (binary, indirect or NotAssigned) from the supplementary file
    - [ ] using `rdfs:subclassOf` or `apid:methodType` ?
        - [ ] check whether all the descendants of an annotated MI class are also annotated
        - [ ] check whether the method type of an MI class is consistent with the type of its descendants
- [ ] handle the `Interaction Identifiers` column
